FROM python:3.9.4-buster
ENV PYTHONUNBUFFERED=1
WORKDIR /code
RUN apt-get install -y libmariadb-dev
RUN pip3 install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . /code/
ENTRYPOINT ["/code/init.sh"]