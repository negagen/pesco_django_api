# PESCO API migrated to Django from PHP

This is a Django project that implements the PESCO API. It is a work in progress.
As of now, its the PHP code just translated to Python. we have a `game` app that is just the WebGL files Unity exports, wrapped in a Django app, The core of the API is in the `api` app.

We use conda for the virtual environment. To create the environment, run:

```
conda env create -f environment.yml
```

To activate the environment, run:

```
conda activate pesco
```

To run the server, run:

```
python manage.py runserver
```

Project developed by [Brandom Vasquez Alcocer](gitlab.com/negagen) and [Anderson Guio](gitlab.com/anderson90804) as part of the [PESCO](https://simon.uis.edu.co/pesco) project.

## TO DO:

- [ ] Dockerfile uses python:3.7-slim-buster and pip install -r requirements.txt, we should use conda instead.
- [ ] Change init script to use the conda python binary instead of the default python binary.
