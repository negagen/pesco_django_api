from typing import Callable
from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE
from django.db.models.fields import CommaSeparatedIntegerField, DateTimeField, FloatField, TextField
from django.db.models.fields.related import ForeignKey
from django.db.models.query import ModelIterable


# Create your models here.
class Sesion(models.Model):
    jugador = models.ManyToManyField(User, through='EstadoSesion')
    nombreSesion = models.CharField(max_length=255)  
    nivelAsignado = models.IntegerField()
    tiempoIteracion = models.IntegerField()
    tiempoMercado = models.IntegerField()

    def __str__(self):
        return self.nombreSesion

class EstadoSesion(models.Model):
    sesion = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    jugador = models.ForeignKey(User, on_delete=models.CASCADE)
    unido = models.BooleanField()
    concluido = models.BooleanField()

    def __str__(self):
        
        return  self.jugador.username + " agregado a " + self.sesion.nombreSesion

class PrecioMercado(models.Model):
    sesion = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    precioMercado = models.FloatField()
    turnoActual = models.IntegerField()
    fechaCambio = models.DateTimeField(auto_now=True)

class OfertasMercado(models.Model):
    sesion = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    coberturaOferta = models.FloatField()
    cantidadActualOferentes = models.IntegerField()
    limiteOferentes = models.IntegerField()
    multa = models.FloatField()
    minOferentes = models.IntegerField()    
    precioCompra = models.FloatField()
    cantidad = models.FloatField()

class MembresiaMercado(models.Model):
    sesion = models.ForeignKey(OfertasMercado, on_delete=CASCADE)
    jugador = models.ForeignKey(User, on_delete=CASCADE)
    unido = models.BooleanField()

class DatosJuego(models.Model):
    jugador = models.ForeignKey(User, on_delete=models.CASCADE)
    sesion = models.ForeignKey(Sesion, on_delete=CASCADE)
    fechaRegistro = DateTimeField(auto_now=True)
    dineroDisponible = models.FloatField()
    inventarioPeces = models.IntegerField()
    inventarioAlimento = models.FloatField()
    tiempoIteracion = models.IntegerField()
    estadoProduccion = models.BooleanField()
    modalidadVenta = models.CharField(max_length=255)
    turnoVenta = models.IntegerField()
    unidoGremio = models.BooleanField()
    pesoPez = models.FloatField()
    totalCostos = models.FloatField()
    consumoReal = models.FloatField()
    coberturaPesoMaximo = models.FloatField()
    factorConversion = models.FloatField()
    incrementoPesoPez = models.FloatField()
    totalAlimentoSuministrado = models.FloatField()
    incrementoGastos = models.FloatField()
    temperatura = models.FloatField()
    racionAlimenticiaRequerida = models.FloatField()
    racionAlimenticiaASuministrar = models.FloatField()
    racionAlimenticiaSuministrada = models.FloatField()
    oxigenoDisuelto = models.FloatField()
    pecesMuertos = models.IntegerField()
    costoInicialPeces = models.FloatField()
    inventarioInicialPeces = models.IntegerField()
    costoMantenimientoDiario = models.FloatField()
    costoTotalAlimento = models.FloatField()
    totalVentas = models.FloatField()
    totalPecesVendidos = models.IntegerField()

class RegistroVentas(models.Model):
    jugador = models.ForeignKey(User, on_delete=models.CASCADE)
    sesion = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    pesoPez = models.FloatField()
    numPeces = models.IntegerField()
    valorVenta = models.FloatField()
    modVenta = models.CharField(max_length=255)
    costo = models.FloatField()
    fechaVenta = models.DateTimeField(auto_now=True)

class DatosSimulacion(models.Model):
    precioActual = models.IntegerField(default=8000)
    turnoActual = models.IntegerField(default=1)
    sesion = models.OneToOneField(Sesion, on_delete=models.CASCADE)
    ventasProductores = models.IntegerField(default=0)
    inventarioOferta = models.FloatField(default=0)
    tiempoIteracion = models.IntegerField(default=0)
    tiempoSimulacion = models.IntegerField(default=0)

class Tabla(models.Model):
    nombreTabla = models.TextField(max_length=255)
    campoX = models.TextField(max_length=255)
    campoY = models.TextField(max_length=255)

    def __str__(self):
        return self.nombreTabla
        
class TablaPunto(models.Model):
    valorX = models.FloatField()
    valorY = models.FloatField()
    tabla = models.ForeignKey(Tabla, on_delete=models.CASCADE)

    def __str__(self):
        return \
            str(self.tabla)+": "+\
            "f("+str(self.valorX)+")="+str(self.valorY)

class RetrasoData(models.Model):
    nombreRetraso = TextField(max_length=50)
    nombreSimulacion = TextField(max_length=50)
    salidaRet1 = FloatField()
    salidaRet2 = FloatField()
    salidaRet3 = FloatField()

    class Meta:
        unique_together = ('nombreRetraso','nombreSimulacion')


