from django.db.models.expressions import OrderBy
import json
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from .models import DatosJuego, EstadoSesion, MembresiaMercado, Sesion, OfertasMercado, RegistroVentas, DatosSimulacion
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from . import services

# Create your views here.
@csrf_exempt
def login_view(request):
    username = request.POST.get('usernamePost', None)
    password = request.POST.get('passwordPost', None)
    user = authenticate(request, username=username, password=password)
    print(user)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        return HttpResponse(status=200)
    else:
        # Return an 'inva lid login' error message.
        return HttpResponse(status=401)

@csrf_exempt
def registro_view(request):
    username = request.POST.get('username', None)
    email = request.POST.get('email', None)
    password = request.POST.get('password', None)
    try:
        user = User.objects.create_user(username, email ,password)
        login(request, user)
        # Redirect to a success page.
        return HttpResponse(status=201)
    except ValueError:
        # Return an 'invalid login' error message.
        return HttpResponse(status=402)

@csrf_exempt
def sesiones_view(request):
    output = ""
    sesiones = Sesion.objects.filter(jugador=request.user)
    if sesiones.count() > 0:
        for sesion in sesiones:
            output += \
                sesion.nombreSesion+"&"+\
                str(sesion.nivelAsignado)+"&"+\
                str(sesion.tiempoIteracion//1000)+"&"+\
                str(sesion.tiempoMercado//1000)+"|"
        return HttpResponse(output)
    else:
        return HttpResponse(status=402)

@csrf_exempt
def ofertas_view(request):
    sesion_name = request.POST.get('sesionPost', None)
    sesion = Sesion.objects.get(nombreSesion = sesion_name)
    ofertas = OfertasMercado.objects.filter(sesion=sesion)
    output = ""
    if ofertas.count() > 0:
        for oferta in ofertas:
            output +=\
                oferta.cantidad+"&"+\
                oferta.precioCompra+"&"+\
                oferta.minOferentes+"&"+\
                oferta.multa+"&"+\
                oferta.limiteOferentes+"&"+ \
                oferta.cantidadActualOferentes+"&"+\
                oferta.coberturaOferta
        return HttpResponse(output)
    else:
        return HttpResponse(status=404)

@csrf_exempt
def loaddata(request):
    sesion_name = request.POST.get('sesionPost',None)
    sesion = Sesion.objects.get(nombreSesion = sesion_name)
    datosJuego = DatosJuego.objects.filter(jugador = request.user, sesion = sesion)
    if datosJuego.count() > 0:
        datos = datosJuego.last()
        datosJson = model_to_dict(datos)
        return JsonResponse(datosJson)
    else:
        return HttpResponse(status=401)

@csrf_exempt
def savedata(request):
    sesion_name = request.POST.get('sesionPost',None)
    print(sesion_name)
    sesion = Sesion.objects.get(nombreSesion = sesion_name)
    dataDict = json.loads(request.POST.get('dataPost',None))
    dataDict.pop('nombreJugador')
    dataDict.pop('sesionJugador')
    DatosJuego.objects.update_or_create(
        jugador = request.user,
        sesion = sesion,
        **dataDict
    )
    return HttpResponse({
        request.user.username+\
        sesion.nombreSesion+\
        request.POST.get('dataPost',None)+\
        str(DatosJuego.objects.filter(jugador=request.user, sesion=sesion).count())
    })

@csrf_exempt
def deletedata(request):
    sesion_name = request.POST.get('sesionPost', None)
    print(sesion_name)
    sesion = Sesion.objects.get(nombreSesion = sesion_name)
    results_game_data = DatosJuego.objects.filter(jugador=request.user, sesion=sesion)
    results_sells_record = RegistroVentas.objects.filter(jugador=request.user, sesion=sesion)
    if results_game_data.count() > 0:
        results_game_data.delete()
        results_sells_record.delete()
        return HttpResponse()
    else:
        return HttpResponse(status=401)    

@csrf_exempt
def mercado(request):
    sesion_name = request.POST.get('sesionPost', None)
    sesion = Sesion.objects.get(nombreSesion = sesion_name)
    datosSimulacion = DatosSimulacion.objects.filter(sesion = sesion)
    if datosSimulacion.count() > 0:
        output = ""
        for datos in datosSimulacion:
            output += str(datos.precioActual) + "|" + str(datos.turnoActual)
        return HttpResponse(output)
    else:
        return HttpResponse(status=404)

@csrf_exempt
def saveventa(request):
    info = json.loads(request.POST.get('dataPost'))
    info.pop('nomSesion')
    info.pop('nomUsuario')
    #username = info.pop('nomUsuario')
    #print(username)
    #user = User.objects.get(username=username)
    user = request.user
    sesion_name = request.POST.get('sesionPost', None)
    sesion = Sesion.objects.get(nombreSesion = sesion_name)    

    modelo = DatosSimulacion.objects.filter(sesion=sesion)
    
    if modelo.count() > 0:

        # Agrega venta a la DB de registro de ventas
        RegistroVentas.objects.create(
            jugador = user,
            sesion = sesion,
            **info
        )

        # Agregar venta al modelo del mercado
        acumuladoVentas = modelo.last().ventasProductores
        print(info)
        totalVentas = acumuladoVentas+(info['numPeces']*info['pesoPez'])
        modelo.update(ventasProductores = totalVentas)

        # Agregar venta al gremio si aplica al caso
        if info['modVenta'] == 'gremio':
            ofertas = OfertasMercado.objects.filter(sesion=sesion)
            if ofertas.count() > 0:
                oferta = ofertas.last()
                acumuladoGremio = oferta.coberturaOferta
                totalGremio = acumuladoGremio + info['valorVenta']
                ofertas.update(coberturaOferta=totalGremio)
        
        return HttpResponse(status=204)
    return HttpResponse()

def turnos(request):
    
    sesion = Sesion.objects.get(nombreSesion=request.POST.get('sesionPost'))
    modalidad = "turno"
    datos = DatosJuego.objects.filter(sesion=sesion, modalidadVenta=modalidad).order_by('-turnoVenta')
    if datos.count() > 0:
    
        output = ""
        for dato in datos:
            output += dato.jugador.username + "&" + dato.turnoVenta + "|"
        return HttpResponse(output)
    else:
    
        return HttpResponse(status=406)

@csrf_exempt
def savelevelwin(request):
   
    user = request.user
    sesion = Sesion.objects.get(nombreSesion=request.POST.get('sesionPost'))
    estadosSesion = EstadoSesion.objects.filter(sesion=sesion, jugador=user)
    if estadosSesion.count()>0:
   
        estadosSesion.update(concluido=True)
        return HttpResponse(status = 206)

def deleteintgremio(request):
    
    user = request.user
    sesion = Sesion.objects.get(nombreSesion=request.POST.get('sesionPost'))
    ofertas = OfertasMercado.objects.filter(sesion=sesion)
    if ofertas.count() > 0:
    
        oferta = ofertas.last()
        MembresiaMercado.objects.filter(sesion=sesion, jugador=user).update(unido=False)

        actualesOferentes = oferta.cantidadActualOferentes
        cantidadOferentes = actualesOferentes -1

        OfertasMercado.objects.filter(sesion=sesion).update(cantidadActualOferentes=cantidadOferentes)
        return HttpResponse(status=102)
    else:
        
        return HttpResponse(status=401)

def addintgremio(request):
    
    user = request.user
    sesion = Sesion.objects.get(nombreSesion=request.POST.get('sesionPost'))
    ofertas = OfertasMercado.objects.filter(sesion=sesion)
    if ofertas.count() > 0:
        
        oferta = ofertas.last()
        MembresiaMercado.objects.update_or_create(sesion=sesion, user=user, unido=True)

        actualesOferentes = oferta.cantidadActualOferentes
        cantidadOferentes = actualesOferentes -1

        OfertasMercado.objects.filter(sesion=sesion).update(cantidadActualOferentes=cantidadOferentes)
        
        return HttpResponse(status=205)
    else:
        
        return HttpResponse(status=401)


def iterarModelo(request):
    listaSimuladores = \
        [
            services.Simulacion(sesion=sesion)\
            for sesion in Sesion.objects.all().order_by('tiempoMercado')\
        ]
    
    for simulador in listaSimuladores:
        simulador.iterar()

    return JsonResponse({
        "output": [model_to_dict(DatosSimulacion.objects.get(sesion=simulacion.sesion)) for simulacion in listaSimuladores]
    })